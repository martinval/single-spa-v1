import React from 'react'
import {navigateToUrl} from 'single-spa'
const NavBar = () => (
  <nav>
    <div className="nav-wrapper">
      <a href="/" onClick={navigateToUrl} className="brand-logo">React Nav Bar</a>
      <ul id="nav-mobile" className="right hide-on-med-and-down">
        <li><a href="/" onClick={navigateToUrl}>Home to React App</a></li>
        <li><a href="/angularJS" onClick={navigateToUrl}>AngularJS App</a></li>
      </ul>
    </div>
  </nav>
)
export default NavBar